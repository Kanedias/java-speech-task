import * as React               from 'react';

import { ReactMic }             from 'react-mic';
import Fab                      from '@material-ui/core/Fab';
import Mic                      from '@material-ui/icons/Mic';
import Stop                     from '@material-ui/icons/Stop';

/**
 * Shows oscillogramme of speech after microphone is allowed to be accessed
 * and controls starting/stopping of the recording.
 */
export class SpeechScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isRecording: false };
  }

  startRecording = () => {
    this.setState({ isRecording: true });
  }

  stopRecording = () => {
    this.setState({ isRecording: false });
  }

  onDataPcm = (pcmBlob) => {
    if (!this.state.isRecording)
      return

    this.props.callback(pcmBlob);
  }

  onStop = (oggBlob) => {
    this.props.callback(new Blob(["stop"]));
    console.log('Stopping recording...');
  }

  render() {
    const { isRecording } = this.state;

    return (
      <div>
        <ReactMic
          record={isRecording}
          className="sound-wave"
          mimeType="audio/ogg"
          onStop={this.onStop}
          onDataPcm={this.onDataPcm}
          strokeColor="#000000"
          backgroundColor="#aaaaaa" />
        <br/>
        <br/>
        <Fab color="secondary" id="micOn"
          disabled={isRecording}
          onClick={this.startRecording}>
          <Mic />
        </Fab>
        <Fab id="micOff"
          disabled={!isRecording}
          onClick={this.stopRecording}>
          <Stop />
        </Fab>
      </div>
    );
  }
}

export default SpeechScreen