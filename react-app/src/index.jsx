import * as React             from "react";
import * as ReactDOM          from "react-dom";

import { withStyles }                       from '@material-ui/core';
import Typography                           from '@material-ui/core/Typography';
import Snackbar                             from '@material-ui/core/Snackbar';
import IconButton                           from '@material-ui/core/IconButton';
import CloseIcon                            from '@material-ui/icons/Close';

import Sockette                             from 'sockette'

import SpeechScreen from "./speech-screen";
import WordsArea from "./words-area"

require ('./styles.css');

/**
 * Main screen showing mic record stream, recognized word list and control buttons
 */
class MainScreen extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      wsConnected: false,
      wsReconnecting: false,
      snackOpen: false,
      statusMsg: "Websocket connection not initiated",
      words: new Map()
    }

    //const wsSourceUrl = window.location.protocol + "//" + window.location.host + "/app";
    //const wsSourceUrl = 'ws://localhost:8080/speech';
    const wsSourceUrl = 'ws://localhost/speech';
    this.ws = new Sockette(wsSourceUrl, {
      timeout: 5000,
      maxAttempts: Infinity,
      onopen: e => { 
        console.log('Ws is connected', e);
        this.setState({wsReconnecting: false, wsConnected: true, snackOpen: true, statusMsg: "Websocket connection established"});
      },
      onreconnect: e => { 
        console.log('Reconnecting  to ws...', e);
        if (!this.state.wsReconnecting) {
          this.setState({wsReconnecting: true, snackOpen: true, statusMsg: "Reconnecting to websocket"}) ;
        }
      },
      onmaximum: e => { 
        console.log('Stop Attempting', e);
        this.setState({wsReconnecting: false });
      },
      onclose: e => { 
        if (!this.state.wsReconnecting) {
          console.log('Disconnected from ws', e);
          this.setState({snackOpen: true, statusMsg: "Websocket connection closed"}) ;
        }
        this.setState({wsConnected: false});
      },
      onmessage: this.handleMessage,
      onerror: e => console.log('Error:', e)
    });
  }

  handleSnackClose = (_event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({ snackOpen: false });
  };

  /**
   * Handles data received from user microphone. Sends data through Websocket connection to the backend
   * for recognition. In case websocket connection is not available, warns the user with persistent
   * notification.
   * 
   * @param pcmData PCM audio data with sampling 16000 (mime: audio/l16)
   * 
   * @see SpeechScreen
   */
  micDataReceived = (pcmData) => {
    if (!this.state.wsConnected) {
      // WebSocket connection is not open, we can't process speech, warn the user
      if (!this.state.snackOpen) {
        this.setState({snackOpen: true, statusMsg: "Can't process speech, websocket connection unavailable"});
      }
      return
    }
    
    // send binary voice data through WebSocket connection
    this.ws.send(pcmData);
  }

  /**
   * Handles data coming from Websocket connected to backend. Data received from server must be 
   * a list of words that were recognized from the stream.
   * 
   * Updates the state of component with words received.
   * 
   * @param msg list of strings representing words
   * 
   * @see WordsArea
   */
  handleMessage = (msg) => {
    let wordList = JSON.parse(msg.data);
    
    // copy all old words
    let currentWordMap = new Map(this.state.words);

    // increase weight of each word present in the wordList
    // leave other words as-is
    wordList.forEach((word) => {
      let currentWeight = currentWordMap.has(word) ? currentWordMap.get(word) : 0;
      currentWordMap.set(word, currentWeight + 1);
    });

    // update state, it will push words down to words area
    this.setState({snackOpen: false, words: currentWordMap});
  }

  render() {
    // snackbar niceties, don't put it too near the bottom line
    const StyledSnackbar = withStyles({
      root: {
        marginBottom: '30px',
      },
    })(Snackbar);

    return (<React.Fragment>
              <Typography component="h2" variant="h1" gutterBottom>
                Java speech recording task
              </Typography>
              <br/>
              <SpeechScreen callback={this.micDataReceived}/>
              <WordsArea words={this.state.words} onClear={() => this.setState({words: new Map()})}/>

              <StyledSnackbar
                anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
                open={this.state.snackOpen}
                autoHideDuration={3000}
                ContentProps={{'aria-describedby': 'message-id'}}
                message={<span id="message-id">{this.state.statusMsg}</span>}
                onClose={this.handleSnackClose}
                action={[
                  <IconButton
                    key="close"
                    aria-label="Close"
                    color="inherit"
                    onClick={this.handleSnackClose}>
                    <CloseIcon/>
                  </IconButton>,
                ]}
              />
            </React.Fragment>);
  }

}

ReactDOM.render(<MainScreen />, document.getElementById("root"));
