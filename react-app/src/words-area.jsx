import * as React               from 'react';

import { withStyles }           from '@material-ui/core/styles';
import { createMuiTheme }       from '@material-ui/core/styles';
import { MuiThemeProvider }     from '@material-ui/core/styles';
import Typography               from '@material-ui/core/Typography';
import Avatar                   from '@material-ui/core/Avatar';
import Grow                     from '@material-ui/core/Grow';
import Chip                     from '@material-ui/core/Chip';
import Card                     from '@material-ui/core/Card';
import Button                   from '@material-ui/core/Button';
import CardActions              from '@material-ui/core/CardActions';
import CardContent              from '@material-ui/core/CardContent';
import red                      from '@material-ui/core/colors/red';

const styles = {
  card: {
    minWidth: 275,
    maxWidth: '40%',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: '30px',
  },

  chip: {
    margin: '5px',
  },

  title: {
    fontSize: 16,
  },

  pos: {
    marginBottom: 12,
  },
};

/**
 * Words area card, contains all recognized words and their weights. Handles clears of the word list as well
 */
export class WordsArea extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  componentDidMount() {
    this.precacheThemes();
  }

  precacheThemes() {
    this.themes = new Map();
    for (let idx = 1; idx < 10; idx++) {
      const color = red[Math.min(idx * 100, 900)];
      const theme = createMuiTheme({
        palette: {
          primary: {
            main: color,
          }
        },
        typography: {
          fontSize: 14,
        },
      });
      this.themes.set(idx, theme);
    }
  }

  render() {
    const { words, classes } = this.props;

    let chips = [];
    words.forEach((value, key, _map) => {
      // colorize words based on weight
      const theme = this.themes.get(Math.min(value, 9))

      // render chips with reveal animation
      chips.push(
          <MuiThemeProvider key={key} theme={theme}>
            <Grow in={true} timeout={1000}>
              <Chip
                avatar={<Avatar>{value}</Avatar>}
                label={key}
                className={classes.chip}
                color="primary"/>
            </Grow>
          </MuiThemeProvider>);
    });

    return (
      <div>
        <Card className={classes.card}>
        
          <CardContent>
            <Typography className={classes.title} color="textSecondary" gutterBottom>
              Recognized word list with weights
            </Typography>
            {chips}
          </CardContent>
          <CardActions>
            <Button size="small" onClick={this.props.onClear}>Clear word list</Button>
          </CardActions>
        </Card>
      </div>
    );
  }
}

export default withStyles(styles)(WordsArea)