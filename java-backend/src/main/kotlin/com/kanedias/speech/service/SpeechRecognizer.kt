package com.kanedias.speech.service

import org.springframework.web.socket.WebSocketSession

/**
 * Interface describing speech recognizer. We might pick up other APIs like Amazon Lex
 * or even OpenCV libraries.
 *
 * Responsible for handling streams of PCM data from a microphone and feeding it to
 * speech recognizing library or cloud API.
 *
 * After words are recognized, should immediately send them back in WebSocket session.
 *
 */
interface SpeechRecognizer: AutoCloseable {

    /**
     * Allocate all required resources, create session/initialize recognition libraries here
     *
     * @param session session to bind recognizer to
     */
    fun init(session: WebSocketSession)

    /**
     * Handle incoming data from microphone. This should be called whenever new data is
     * available, even if the speaker is silent as some APIs expect sound to be coming
     * non-stop.
     *
     * @param msg raw PCM data in 16khz
     */
    fun handleMicData(msg: ByteArray)

    /**
     * Report list of collected words back. This clears the collected word list,
     * so you shouldn't worry of same words being reported twice.
     *
     * Note: This shouldn't necessarily be requested in the same thread as mic data.
     *
     * @return list containing words deduced from the session so far
     */
    fun reportWords(): List<String>
}