package com.kanedias.speech.service

import com.google.gson.Gson
import com.kanedias.speech.GeneralConfig.Companion.RECOGNIZER_SCHEDULER_QUAL
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.web.socket.BinaryMessage
import org.springframework.web.socket.CloseStatus
import org.springframework.web.socket.WebSocketSession
import org.springframework.web.socket.handler.BinaryWebSocketHandler

import com.kanedias.speech.WebSocketConfig
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.scheduling.TaskScheduler
import org.springframework.web.socket.TextMessage
import java.time.Duration
import java.util.concurrent.ScheduledFuture

/**
 * Handles WebSocket connection updates and incoming binary messages. Messages ought to contain
 * mic PCM binary data for later processing by [GoogleSpeechStreamRecognizer].
 *
 * @see WebSocketConfig
 * @see GoogleSpeechStreamRecognizer
 */
class MicWsHandler : BinaryWebSocketHandler() {

    companion object {
        const val RECOGNIZER_KEY = "stream-recognizer"
        const val TASK_KEY = "stream-recognizer-task"
        private val LOGGER = LoggerFactory.getLogger(MicWsHandler::class.java)
    }

    @Autowired
    lateinit var ctx: ApplicationContext

    @Qualifier(value = RECOGNIZER_SCHEDULER_QUAL)
    @Autowired
    lateinit var scheduler: TaskScheduler

    override fun afterConnectionEstablished(session: WebSocketSession) {
        LOGGER.info("WebSocket connection established, session {}", session)

        // initialize recognizer and store it in session
        val recognizer = ctx.getBean(SpeechRecognizer::class.java)
        recognizer.init(session)

        // prepare reporting routine that will be executed periodically
        val reporter = {
            val collected = recognizer.reportWords()

            if (session.isOpen && collected.isNotEmpty()) {
                val pingBack = TextMessage(Gson().toJson(collected))
                session.sendMessage(pingBack)
            }
        }

        val task = scheduler.scheduleAtFixedRate(reporter, Duration.ofSeconds(1))
        session.attributes[RECOGNIZER_KEY] = recognizer
        session.attributes[TASK_KEY] = task
    }

    override fun afterConnectionClosed(session: WebSocketSession, status: CloseStatus) {
        LOGGER.info("Closing web socket session {}", session.id)

        // free resources used by recognizer, close speech stream
        val recognizer = session.attributes.remove(RECOGNIZER_KEY) as SpeechRecognizer
        recognizer.close()

        // stop scheduled task from running for this recognizer
        val task = session.attributes.remove(TASK_KEY) as ScheduledFuture<*>
        task.cancel(false)
    }

    override fun handleBinaryMessage(session: WebSocketSession, message: BinaryMessage) {
        LOGGER.debug("Handling binary message {} from session {}", message, session)

        // recognizer should already be initialized as session is created
        val recognizer = session.attributes[RECOGNIZER_KEY] as GoogleSpeechStreamRecognizer
        recognizer.handleMicData(message.payload.array())
    }
}