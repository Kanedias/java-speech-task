package com.kanedias.speech.service

import com.google.api.gax.core.FixedCredentialsProvider
import com.google.api.gax.rpc.ClientStream
import com.google.protobuf.ByteString
import com.google.api.gax.rpc.ResponseObserver
import com.google.api.gax.rpc.StreamController
import com.google.auth.oauth2.ServiceAccountCredentials
import com.google.cloud.speech.v1p1beta1.*
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.config.ConfigurableBeanFactory
import org.springframework.context.annotation.Primary
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Service
import org.springframework.web.socket.WebSocketSession
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicReference

/**
 * Responsible for handling streams of PCM data from a microphone.
 *
 * This could be a raw class but implemented as scoped bean as later
 * we may need to track it in Spring's application context.
 *
 */
@Primary
@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
class GoogleSpeechStreamRecognizer: SpeechRecognizer {

    companion object {
        const val CREDENTIALS_FILE = "/speech-api-account.json"
        private val LOGGER = LoggerFactory.getLogger(GoogleSpeechStreamRecognizer::class.java)
    }

    private lateinit var session: WebSocketSession

    private lateinit var speechClient: SpeechClient
    private lateinit var speechStream: ClientStream<StreamingRecognizeRequest>

    private var streamStartTime = 0L
    private var handshakeCompleted = AtomicBoolean()

    private var collectedWords = AtomicReference<List<String>>(emptyList())

    override fun init(session: WebSocketSession) {
        this.session = session
    }

    private fun populateAppSettings(): SpeechSettings {
        val credStream = ServiceAccountCredentials.fromStream(javaClass.getResourceAsStream(CREDENTIALS_FILE))
        val credProvider = FixedCredentialsProvider.create(credStream)
        return SpeechSettings.newBuilder()
                .setCredentialsProvider(credProvider)
                .build()
    }

    /**
     * First message in audio stream must be the configuration of the stream.
     */
    private fun sendHandshake() {
        val request = StreamingRecognizeRequest.newBuilder()
                .setStreamingConfig(StreamingRecognitionConfig.newBuilder()
                        .setConfig(RecognitionConfig.newBuilder()
                                .setEncoding(RecognitionConfig.AudioEncoding.LINEAR16) // client handles downsampling
                                .setSampleRateHertz(16000) // most applicable for recognizing
                                .setEnableWordTimeOffsets(true)
                                .setLanguageCode("ru-RU")
                                .addAllAlternativeLanguageCodes(listOf("en-US")) // what, they could play this trick on me, right?
                                .build())
                        .setInterimResults(true) // we want to receive words as fast as possible
                        .build())
                .build()

        speechStream.send(request)
    }

    override fun handleMicData(msg: ByteArray) {
        // Small hack: handle binary message as text. This poses low overhead generally
        // and creating distinct TextWebSocketHandler just to receive one command
        // is overkill for now. In case app will grow this part should be rethinked
        if (msg.size == 4 && msg.toString(Charsets.UTF_8) == "stop") {
            LOGGER.info("Closing by server command from session {}", session)
            close()
            return
        }

        // Called the first time and
        if (handshakeCompleted.compareAndSet(false, true)) {
            LOGGER.info("(Re)initializing Speech API session")
            speechClient = SpeechClient.create(populateAppSettings())
            speechStream = speechClient.streamingRecognizeCallable().splitCall(StreamResponseHandler())
            streamStartTime = System.currentTimeMillis()
            sendHandshake()
        }

        try {
            val request = StreamingRecognizeRequest.newBuilder()
                    .setAudioContent(ByteString.copyFrom(msg))
                    .build()
            speechStream.send(request)
        } catch (e: Exception) {
            LOGGER.info("Error in session {}", session.id, e)
        }

        // Google API can only handle 65 seconds of recognizing stream, then it breaks
        // We have to re-init stream on the fly after 60 seconds or we'll loose the words
        if (System.currentTimeMillis() - streamStartTime > 60_000) {
            LOGGER.info("Closing stream not to exceed Speech API timeout")
            close()
        }
    }

    override fun reportWords(): List<String> {
        return collectedWords.getAndSet(emptyList())
    }

    override fun close() {
        if (handshakeCompleted.compareAndSet(true, false)) {
            speechStream.closeSend()
            speechClient.close()
        }
    }

    /**
     * Lives in Google Speech Library-owned thread pool and periodically reports
     * results of stream recognition back to the caller
     */
    inner class StreamResponseHandler: ResponseObserver<StreamingRecognizeResponse> {

        var prevTranscript = emptyList<String>()

        override fun onStart(controller: StreamController) {
            LOGGER.debug("Started listening to speech API responses, session  {}", session.id)
        }

        override fun onResponse(response: StreamingRecognizeResponse) {
            val result = response.resultsList.firstOrNull() ?: return
            val alternative = result.alternativesList?.firstOrNull() ?: return
            LOGGER.info("Transcript for session {}: {}", session, alternative.transcript)

            // have data, parse transcript
            val approximation = alternative.transcript.split(" ")

            // try to filter-out interim results
            val cleaned = (approximation - prevTranscript).filter { it.isNotBlank() }.map { it.toLowerCase() }
            prevTranscript = approximation

            collectedWords.set(collectedWords.get() + cleaned)
        }

        override fun onComplete() {
            LOGGER.debug("Not listening to speech API updates anymore, session {}", session.id)
        }

        override fun onError(t: Throwable) {
            LOGGER.error("Error in speech stream handler for session {}", session.id, t)
        }
    }

}