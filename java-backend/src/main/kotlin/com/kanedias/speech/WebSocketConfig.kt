package com.kanedias.speech

import com.kanedias.speech.service.MicWsHandler
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.socket.config.annotation.EnableWebSocket
import org.springframework.web.socket.WebSocketHandler
import org.springframework.web.socket.config.annotation.WebSocketConfigurer
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry

/**
 * We configure RAW WebSocket handler, without SockJS and STOMP.
 * Only this way can we support handling binary messages.
 */
@Configuration
@EnableWebSocket
class WebSocketConfig: WebSocketConfigurer {

    override fun registerWebSocketHandlers(registry: WebSocketHandlerRegistry) {
        registry.addHandler(micData(), "/speech").setAllowedOrigins("*")
    }

    @Bean
    fun micData(): WebSocketHandler {
        return MicWsHandler()
    }
}