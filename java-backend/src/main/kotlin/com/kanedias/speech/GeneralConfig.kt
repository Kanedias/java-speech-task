package com.kanedias.speech

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler


@Configuration
class GeneralConfig {

    companion object {
        const val RECOGNIZER_SCHEDULER_QUAL = "recognizer-scheduler"
    }

    @Qualifier(value = RECOGNIZER_SCHEDULER_QUAL)
    @Bean
    fun recognizerTaskScheduler(): ThreadPoolTaskScheduler {
        return ThreadPoolTaskScheduler().apply {
            poolSize = 5
            threadNamePrefix = RECOGNIZER_SCHEDULER_QUAL
        }
    }

}