Java Speech Task
==================

A task I received from one small but proud company.

What it does
-------------

* Records your voice in a browser and feeds it to Google Speech API
* Gets back transcript and transforms it to a list of words
* Feeds words back to a web client and shows them with relative weights on a screen

Requirements
------------

- Unrestricted Internet connection
- GnuPG and Docker Compose installed
- Unbound port 80
- Microphone
- Firefox (or Chrome)
- (probably) Linux desktop, didn't test it anywhere else

Installation
------------

You need to unencrypt file with Google API Account

    $ cd java-backend/src/main/resources
    $ gpg --decrypt speech-api-account.json.gpg > speech-api-account.json
    $ # it will ask for password, you should have received it from me

Running
-------

Just run

    $ docker-compose up --build

And wait while all components are started (i.e. stopped spamming messages to your console)  
Then just head to http://localhost and see how it looks like.

Struggles
---------

A number of issues I've bumped into:

- React-Mic: wasn't able to record PCM frames or downsample them, had to [fork it](https://github.com/Adonai/react-mic)
- Google Speech API doesn't handle interim values very well, had to do additional processing just for it to be more sane
- Spring Boot only has STOMP and SockJS documented, both are text-based, had to create raw binary websocket handler
- Weights of words aren't a thing that was requested by a task authors but just a nice way to play with Material UI Themes

License
--------

    Copyright (C) 2018  Oleg `Kanedias` Chernovskiy

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

